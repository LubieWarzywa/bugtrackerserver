﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BugTracker.Models
{
    public class Tester
    {
        public virtual ICollection<Bug> Bug { get; set; }
        public virtual ICollection<Project> Project { get; set; }
    }
}