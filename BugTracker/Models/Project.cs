﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System;

namespace BugTracker.Models
{
    public class Project
    {
        [ScaffoldColumn(false)]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Code { get; set; }

        public ICollection<Bug> Bug { get; set; }

        public ICollection<Programmer> Programmer { get; set; }
    }
}