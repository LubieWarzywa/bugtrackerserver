﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BugTracker.Models
{
    public class Programmer
    {
         [ScaffoldColumn(false)]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
         [ScaffoldColumn(false)]
        public string ProjectID { get; set; }

        public virtual Project Project { get; set; }

        public ICollection<Bug> Bug { get; set; }

    }
}