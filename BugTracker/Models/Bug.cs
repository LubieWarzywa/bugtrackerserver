﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BugTracker.Models
{
      using System.ComponentModel.DataAnnotations;

    public class Bug
    {
        [ScaffoldColumn(false)]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string State { get; set; }
        [Required]
        public string Priority { get; set; }
        [Required]
        public System.DateTime Date { get; set; }
         [ScaffoldColumn(false)]
        public string ProjectID { get; set; }
         [ScaffoldColumn(false)]
        public string ProgrammerID { get; set; }

        public virtual Project Project { get; set; }

        public virtual Programmer Programmer { get; set; }


    }
}